package org.nielsen.json.store;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.List;
import java.util.Scanner;

import com.google.gson.Gson;

public class StoreReference {
	private StoreRefData StoreRefData;

	public static void main(String[] args) {
		Scanner fileScanner = null;
		try {
			fileScanner = new Scanner(new File("StoreDetails"));
			String storeDetails = fileScanner.useDelimiter("\\Z").next();
			Gson gson = new Gson();
			StoreReference obj = gson.fromJson(storeDetails, StoreReference.class);
			System.out.println(gson.toJson(obj));
		} catch (FileNotFoundException e) {
			throw new RuntimeException(e.getMessage());
		} finally {
			if (fileScanner != null) {
				fileScanner.close();
			}
		}
	}

	public static StoreRefData getStoreRefDataFromJSONString(String jsonObj) {
		Gson gson = new Gson();
		StoreReference obj = gson.fromJson(jsonObj, StoreReference.class);
		return obj.getStoreRefData();
	}

	public StoreRefData getStoreRefData() {
		return StoreRefData;
	}

	public String retrieveAtLeastOneStoreCode() {
		String storeCode = null;
		if (StoreRefData != null) {
			storeCode = retrieveAtLeastOneStoreCode(StoreRefData.getStores());
		}
		return storeCode;
	}

	private String retrieveAtLeastOneStoreCode(List<StoreDetail> stores) {
		String storeCode = null;
		if (stores != null && stores.size() > 0) {
			for (StoreDetail store : stores) {
				if (store != null && store.getStoreCode() != null) {
					storeCode = store.getStoreCode();
					break;
				}
			}
		}
		return storeCode;
	}
}
