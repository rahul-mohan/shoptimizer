package org.nielsen.json.store;

import java.util.List;

public class StoreInfo {
	private List<StoreCharacteristic> Characteristics;

	public List<StoreCharacteristic> getStoreCharacteristics() {
		return Characteristics;
	}

	public void setStoreCharacteristics(List<StoreCharacteristic> characteristics) {
		Characteristics = characteristics;
	}
}
