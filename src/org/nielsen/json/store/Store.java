package org.nielsen.json.store;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.List;
import java.util.Scanner;

import com.google.gson.Gson;

public class Store {
	private StoreInfo StoreInfo;

	public static void main(String[] args) {
		Scanner fileScanner = null;
		try {
			fileScanner = new Scanner(new File("StoreInfo"));
			String storeDetails = fileScanner.useDelimiter("\\Z").next();
			Gson gson = new Gson();
			Store obj = gson.fromJson(storeDetails, Store.class);
			System.out.println(gson.toJson(obj));
		} catch (FileNotFoundException e) {
			throw new RuntimeException(e.getMessage());
		} finally {
			if (fileScanner != null) {
				fileScanner.close();
			}
		}
	}

	public StoreInfo getStoreInfo() {
		return StoreInfo;
	}

	public void setStoreInfo(StoreInfo storeInfo) {
		StoreInfo = storeInfo;
	}

	public StoreCoordinate getStoreCoordinate() {
		StoreCoordinate coordinate = null;
		if (StoreInfo != null) {
			coordinate = getStoreCoordinate(StoreInfo.getStoreCharacteristics());
		}
		return coordinate;
	}

	public StoreCoordinate getStoreCoordinate(List<StoreCharacteristic> sc) {
		StoreCoordinate coordinate = null;
		if (sc != null && sc.size() > 0) {
			for (StoreCharacteristic store : sc) {
				if (store != null && store.getLatitude() != null && store.getLongitude() != null) {
					coordinate = new StoreCoordinate();
					coordinate.setLatitude(store.getLatitude());
					coordinate.setLongitude(store.getLongitude());
					break;
				}
			}
		}
		return coordinate;
	}
}
