package org.nielsen.json.store;

public class StoreCharacteristic {
	private String LATITUDE;
	private String LONGITUDE;
	private String TDLINX_STORE_CODE;

	public String getLatitude() {
		return LATITUDE;
	}
	
	public void setLatitude(String latitude) {
		LATITUDE = latitude;
	}

	public String getLongitude() {
		return LONGITUDE;
	}
	
	public void setLongitude(String longitude) {
		LONGITUDE = longitude;
	}

	public String getStoreCode() {
		return TDLINX_STORE_CODE;
	}

	public void setStoreCode(String storeCode) {
		TDLINX_STORE_CODE = storeCode;
	}
}
