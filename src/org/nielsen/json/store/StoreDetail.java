package org.nielsen.json.store;

public class StoreDetail {
	private String StoreName;
	private String OwnerName;
	private String StoreCode;
	private String StoreStatus;
	private String StoreNumber;

	public String getStoreName() {
		return StoreName;
	}
	public void setStoreName(String storeName) {
		StoreName = storeName;
	}
	public String getOwnerName() {
		return OwnerName;
	}
	public void setOwnerName(String ownerName) {
		OwnerName = ownerName;
	}
	public String getStoreCode() {
		return StoreCode;
	}
	public void setStoreCode(String storeCode) {
		StoreCode = storeCode;
	}
	public String getStoreStatus() {
		return StoreStatus;
	}
	public void setStoreStatus(String storeStatus) {
		StoreStatus = storeStatus;
	}
	public String getStoreNumber() {
		return StoreNumber;
	}
	public void setStoreNumber(String storeNumber) {
		StoreNumber = storeNumber;
	}
}
