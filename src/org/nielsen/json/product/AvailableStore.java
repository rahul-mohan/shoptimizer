package org.nielsen.json.product;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class AvailableStore {
	private List<Retailer> Retailer;

	public Retailer retrieveRetailer(String upcCode, String storeId) {
		Retailer retailer = null;
		if (Retailer != null && Retailer.size() > 0) {
			for (Retailer store : Retailer) {
				if (storeId.equals(store.getStoreId())) {
					return store;
				}
			}
		}		
		return retailer;
	}

	public Set<String> buildUniqueStores() {
		Set<String> uniqueStores = new HashSet<String>();
		if (Retailer != null && Retailer.size() > 0) {
			for (Retailer store : Retailer) {
				uniqueStores.add(store.getStoreId());
			}
		}
		
		return uniqueStores;
	}

	public List<Retailer> getRetailers() {
		return Retailer;
	}

	public void setRetailers(List<Retailer> retailer) {
		Retailer = retailer;
	}
}
