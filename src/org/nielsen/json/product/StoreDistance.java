package org.nielsen.json.product;

public class StoreDistance {
	private String storeId;
	private float distance;

	public StoreDistance(String storeId, float distance) {
		this.storeId = storeId;
		this.distance = distance;
	}

	public StoreDistance setStoreIdAndDistance(String storeId, float distance) {
		this.storeId = storeId;
		this.distance = distance;
		return this;
	}

	public String getStoreId() {
		return storeId;
	}

	public void setStoreId(String storeId) {
		this.storeId = storeId;
	}

	public float getDistance() {
		return distance;
	}

	public void setDistance(float distance) {
		this.distance = distance;
	}
}
