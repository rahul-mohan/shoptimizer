package org.nielsen.json.product;


public class ProductDetail {
	private String UPC;
	private String Description;
	private String Item_Code;
	private String Module;
	private int Rank;

	public String getUPC() {
		return UPC;
	}

	public void setUPC(String upc) {
		this.UPC = upc;
	}

	public String getDescription() {
		return Description;
	}

	public void setDescription(String description) {
		this.Description = description;
	}

	public String getItemCode() {
		return Item_Code;
	}

	public void setItemCode(String itemCode) {
		this.Item_Code = itemCode;
	}

	public String getModule() {
		return Module;
	}

	public void setModule(String module) {
		this.Module = module;
	}

	public int getRank() {
		return Rank;
	}

	public void setRank(int rank) {
		this.Rank = rank;
	}

	@Override
	public String toString() {
		return "ProductDetail [UPC=" + UPC + ", Description=" + Description
				+ ", Item_Code=" + Item_Code + ", Module=" + Module + ", Rank="
				+ Rank + "]";
	}
}
