package org.nielsen.json.product;

public class StorePrice {
	private String storeId;
	private float price;

	public StorePrice(String storeId, float price) {
		this.storeId = storeId;
		this.price = price;
	}
	
	public StorePrice setStoreIdAndPrice(String storeId, float price) {
		this.storeId = storeId;
		this.price = price;
		return this;
	}

	public String getStoreId() {
		return storeId;
	}

	public void setStoreId(String storeId) {
		this.storeId = storeId;
	}

	public float getPrice() {
		return price;
	}

	public void setPrice(float price) {
		this.price = price;
	}
}
