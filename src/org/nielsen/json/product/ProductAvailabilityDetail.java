package org.nielsen.json.product;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;

public class ProductAvailabilityDetail {
	private String Brand;
	private String Category;
	private String Manufacturer;
	private String ProductName;
	private String Size;
	private String BarCode;
	private List<AvailableStore> Availability;

	public static Set<String> buildUniqueStoresById(List<ProductAvailabilityDetail> stores) {
		Set<String> uniqueStores = new HashSet<String>();
		if (stores != null && stores.size() > 0) {
			for (ProductAvailabilityDetail store : stores) {
				uniqueStores.addAll(store.buildUniqueStoresById());
			}
		}
		return uniqueStores;
	}

	public Retailer retrieveRetailer(String upcCode, String storeId) {
		Retailer retailer = null;
		if (BarCode != null && BarCode.equals(upcCode) && Availability != null && Availability.size() > 0) {
			for (AvailableStore store : Availability) {
				retailer = store.retrieveRetailer(upcCode, storeId);
				if (retailer != null) {
					return retailer;
				}
			}
		}
		return retailer;
	}

	public List<Retailer> retrieveAllRetailers() {
		List<Retailer> allRetailers = new ArrayList<Retailer>();
		if (Availability != null) {
			for (AvailableStore store : Availability) {
				if (store != null && CollectionUtils.isNotEmpty(store.getRetailers())) {
					allRetailers.addAll(store.getRetailers());
				}
			}
		}

		return allRetailers;
	}

	public Set<String> buildUniqueStoresById() {
		Set<String> uniqueStores = new HashSet<String>();
		if (Availability != null && Availability.size() > 0) {
			uniqueStores.addAll(buildUniqueStores(Availability));
		}
		return uniqueStores;
	}

	private Set<String> buildUniqueStores(List<AvailableStore> stores) {
		Set<String> uniqueStores = new HashSet<String>();
		if (stores != null && stores.size() > 0) {
			for (AvailableStore store : stores) {
				uniqueStores.addAll(store.buildUniqueStores());
			}
		}
		return uniqueStores;
	}

	public String getBrand() {
		return Brand;
	}

	public void setBrand(String brand) {
		Brand = brand;
	}

	public String getCategory() {
		return Category;
	}

	public void setCategory(String category) {
		Category = category;
	}

	public String getManufacturer() {
		return Manufacturer;
	}

	public void setManufacturer(String manufacturer) {
		Manufacturer = manufacturer;
	}

	public String getProductName() {
		return ProductName;
	}

	public void setProductName(String productName) {
		ProductName = productName;
	}

	public String getSize() {
		return Size;
	}

	public void setSize(String size) {
		Size = size;
	}

	public String getBarCode() {
		return BarCode;
	}

	public void setBarCode(String barCode) {
		BarCode = barCode;
	}

	public List<AvailableStore> getAvailabileStores() {
		return Availability;
	}

	public void setAvailabileStores(List<AvailableStore> availabileStores) {
		Availability = availabileStores;
	}
}
