package org.nielsen.json.product;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.List;
import java.util.Scanner;

import com.google.gson.Gson;

public class ProductDetails {
	private List<ProductDetail> ProductDetails;

	public static void main(String[] args) {
		Scanner fileScanner = null;
		try {
			fileScanner = new Scanner(new File("ProductDetails"));
			String productDetails = fileScanner.useDelimiter("\\Z").next();
			Gson gson = new Gson();
			ProductDetails obj = gson.fromJson(productDetails, ProductDetails.class);
			System.out.println(gson.toJson(obj));
		} catch (FileNotFoundException e) {
			throw new RuntimeException(e.getMessage());
		} finally {
			if (fileScanner != null) {
				fileScanner.close();
			}
		}
	}

	public static List<ProductDetail> getProductDetailsFromJSONString(String productDetails) {
		Gson gson = new Gson();
		ProductDetails obj = gson.fromJson(productDetails, ProductDetails.class);
		return obj.getProductDetails();
	}

	public List<ProductDetail> getProductDetails() {
		return ProductDetails;
	}

	public void setProductDetails(List<ProductDetail> productDetails) {
		ProductDetails = productDetails;
	}

	@Override
	public String toString() {
		return "ProductDetails [ProductDetails=" + ProductDetails + "]";
	}
}
