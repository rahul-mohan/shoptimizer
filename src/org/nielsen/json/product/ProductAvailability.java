package org.nielsen.json.product;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

import com.google.gson.Gson;

public class ProductAvailability {
	private ProductAvailabilityDetail Product;

	public static void main(String[] args) {
		Scanner fileScanner = null;
		try {
			fileScanner = new Scanner(new File("ProductAvailabilityDetails"));
			String productAvailabilityDetails = fileScanner.useDelimiter("\\Z").next();
			Gson gson = new Gson();
			ProductAvailability obj = gson.fromJson(productAvailabilityDetails, ProductAvailability.class);
			System.out.println(gson.toJson(obj));
		} catch (FileNotFoundException e) {
			throw new RuntimeException(e.getMessage());
		} finally {
			if (fileScanner != null) {
				fileScanner.close();
			}
		}
	}

	public static ProductAvailabilityDetail retrieveProductAvailabilityDetails(String availabilityDetails) {
		Gson gson = new Gson();
		ProductAvailability productAvailability = gson.fromJson(availabilityDetails, ProductAvailability.class);
		return productAvailability.getProductAvailabilityDetail();
	}

	public ProductAvailabilityDetail getProductAvailabilityDetail() {
		return Product;
	}
}
