package org.nielsen.json.product;

import org.springframework.util.StringUtils;

public class Retailer {
	private String RetailerName;
	private String StoreId;
	private String StoreName;
	private String StoreNumber;
	private String Distance;
	private float price;
	private RetailerLocation Locations;

	public float getPrice() {
		return price;
	}

	public void setPrice(float price) {
		this.price = price;
	}

	public RetailerLocation getLocations() {
		return Locations;
	}

	public void setLocations(RetailerLocation locations) {
		Locations = locations;
	}

	public String getRetailerName() {
		return RetailerName;
	}

	public void setRetailerName(String retailerName) {
		RetailerName = retailerName;
	}

	public String getStoreId() {
		return StoreId;
	}

	public void setStoreId(String storeId) {
		StoreId = storeId;
	}

	public String getStoreName() {
		return StoreName;
	}

	public void setStoreName(String storeName) {
		StoreName = storeName;
	}

	public String getStoreNumber() {
		return StoreNumber;
	}

	public void setStoreNumber(String storeNumber) {
		StoreNumber = storeNumber;
	}

	public String getDistance() {
		return Distance;
	}

	public float getDistanceAsFloat() {
		if (StringUtils.hasText(Distance)) {
			return Float.valueOf(Distance);
		} else {
			return Float.valueOf("0.0");
		}
	}

	public void setDistance(String distance) {
		Distance = distance;
	}

	public RetailerLocation getRetailerLocation() {
		return Locations;
	}

	public void setRetailerLocation(RetailerLocation retailerLocation) {
		Locations = retailerLocation;
	}
}
