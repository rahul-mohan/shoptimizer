package org.nielsen.json.location;

import java.util.List;

public class AddressComponents {
	private List<AddressComponent> address_components;

	public List<AddressComponent> getAddressComponents() {
		return address_components;
	}

	public void setAddressComponents(List<AddressComponent> address_components) {
		this.address_components = address_components;
	}	
}
