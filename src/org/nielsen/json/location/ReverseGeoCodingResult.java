package org.nielsen.json.location;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.List;
import java.util.Scanner;

import com.google.gson.Gson;

public class ReverseGeoCodingResult {
	private static final String POSTAL_CODE = "postal_code";
	private List<AddressComponents> results;

	public static void main(String[] args) {
		Scanner fileScanner = null;
		try {
			fileScanner = new Scanner(new File("ReverseGeoCoding"));
			String results = fileScanner.useDelimiter("\\Z").next();
			Gson gson = new Gson();
			ReverseGeoCodingResult obj = gson.fromJson(results, ReverseGeoCodingResult.class);
			System.out.println(gson.toJson(obj));
			System.out.println("Zip Code: " + obj.retrieveZipCode());
		} catch (FileNotFoundException e) {
			throw new RuntimeException(e.getMessage());
		} finally {
			if (fileScanner != null) {
				fileScanner.close();
			}
		}
	}

	public String retrieveZipCode() {
		String zipCode = null;
		if (results != null) {
			for (AddressComponents component : results) {
				zipCode = retrieveZipCode(component.getAddressComponents());
				if (zipCode != null) {
					break;
				}
			}
		}
		
		return zipCode;
	}
	
	private String retrieveZipCode( List<AddressComponent> addresses) {
		String zipCode = null;
		if (addresses != null && addresses.size() > 0) {
			for (AddressComponent address : addresses) {
				zipCode = retrieveZipCode(address);
				if (zipCode != null) {
					break;
				}				
			}
		}		
		return zipCode;
	}
	
	private String retrieveZipCode(AddressComponent address) {
		String zipCode = null;
		if (address != null && address.getTypes() != null) {
			for (String type : address.getTypes()) {
				if (POSTAL_CODE.equals(type)) {
					zipCode = address.getShortName();
					break;
				}
			}
		}		
		return zipCode;
	}
}
