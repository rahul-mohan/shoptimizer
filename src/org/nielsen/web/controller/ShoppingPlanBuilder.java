package org.nielsen.web.controller;

import org.nielsen.domain.plan.ShoppingPlan;
import org.nielsen.domain.plan.ShoppingPlans;
import org.nielsen.service.composite.LocalStoreProductCompositeService;
import org.nielsen.service.composite.ShoppingPlanCompositeService;
import org.nielsen.service.composite.request.ShoppingCartCompositeRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;

@Controller
public class ShoppingPlanBuilder {

	@Autowired
	private LocalStoreProductCompositeService lcpcService;

	@Autowired
	private ShoppingPlanCompositeService spcService;

	@RequestMapping(value = "/buildShoppingPlanMock", method = RequestMethod.GET)
	public @ResponseBody
	String buildShoppingPlanOld(@RequestBody ShoppingCartCompositeRequest request) {
		ShoppingPlans plans = buildMockPlans();
		return plans.toJson();
	}

	@RequestMapping(value = "/buildShoppingPlanPost", consumes = "application/json", method = RequestMethod.POST)
	public @ResponseBody
	ShoppingPlans buildShoppingPlan(@RequestBody ShoppingCartCompositeRequest request) {
		return getSpcService().createShoppingPlans(request);
	}

	@RequestMapping(value = "/buildShoppingPlan", method = RequestMethod.GET)
	public @ResponseBody
	ShoppingPlans buildShoppingPlan(@RequestParam String payload) {
		try {
			ShoppingCartCompositeRequest request = (new Gson()).fromJson(payload, ShoppingCartCompositeRequest.class);
			return getSpcService().createShoppingPlans(request);
		} catch(Exception e){
			return buildMockPlans();
		}
	}
	
	private ShoppingPlans buildMockPlans() {
		// mock data
		ShoppingPlans plans = new ShoppingPlans();

		plans.addPlan(
				new ShoppingPlan().setPlanName("Save Money").setTotalAvailability(2).setTotalCost(205.45f)
						.setTotalDistance(12.25f).setTotalStores(3))
				.addPlan(
						new ShoppingPlan().setPlanName("Save Time").setTotalAvailability(1).setTotalCost(245.45f)
								.setTotalDistance(7.3f).setTotalStores(1))
				.addPlan(
						new ShoppingPlan().setPlanName("Save Gas").setTotalAvailability(1).setTotalCost(215.45f)
								.setTotalDistance(4.1f).setTotalStores(1));

		return plans;
	}

	public LocalStoreProductCompositeService getLcpcService() {
		if (lcpcService == null) {
			// For non spring hookup
			lcpcService = new LocalStoreProductCompositeService();
		}
		return lcpcService;
	}

	public ShoppingPlanCompositeService getSpcService() {
		if (spcService == null) {
			// For non spring hookup
			spcService = new ShoppingPlanCompositeService();
		}
		return spcService;
	}
}
