package org.nielsen.web.controller;

import org.nielsen.service.composite.LocalStoreProductCompositeService;
import org.nielsen.service.composite.reponse.ShoppingCartCompositeResponse;
import org.nielsen.service.composite.request.ShoppingCartCompositeRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class ShoppingCartController {

	@Autowired
	private LocalStoreProductCompositeService lcpcService;

	@RequestMapping(value = "process", method = RequestMethod.POST)
	public @ResponseBody
	ShoppingCartCompositeResponse processShoppingCart(@RequestBody ShoppingCartCompositeRequest request) {
		if (!StringUtils.isEmpty(request.getZipCode())) {
			return getLcpcService().retrieveSortedLocalStoreProductsByUpcCodes(request.getUpcCodes(),
					request.getZipCode(), request.getSearchRadius());
		} else {
			return getLcpcService().retrieveSortedLocalStoreProductsByUpcCodes(request.getUpcCodes(),
					request.getLatitude(), request.getLongitude(), request.getSearchRadius());
		}
	}

	@RequestMapping(value = "hello")
	public @ResponseBody
	String hello() {
		return "Hello";
	}

	public LocalStoreProductCompositeService getLcpcService() {
		if (lcpcService == null) {
			// For non spring hookup
			lcpcService = new LocalStoreProductCompositeService();
		}
		return lcpcService;
	}
}
