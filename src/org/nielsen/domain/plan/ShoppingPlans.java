package org.nielsen.domain.plan;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;

public class ShoppingPlans {
	private List<ShoppingPlan> plans;
	private boolean success;
	private String message;
	// Only populated if zipcode is passed
	private String convertedLatitude;
	// Only populated if zipcode is passed
	private String convertedLongitude;	

	public ShoppingPlans() {
		plans = new ArrayList<ShoppingPlan>();
	}

	public ShoppingPlans addPlan(ShoppingPlan plan) {
		plans.add(plan);
		return this;
	}

	public String toJson() {
		return (new Gson()).toJson(this);
	}

	public List<ShoppingPlan> getPlans() {
		return plans;
	}

	public void setPlans(List<ShoppingPlan> plans) {
		this.plans = plans;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getConvertedLatitude() {
		return convertedLatitude;
	}

	public void setConvertedLatitude(String convertedLatitude) {
		this.convertedLatitude = convertedLatitude;
	}

	public String getConvertedLongitude() {
		return convertedLongitude;
	}

	public void setConvertedLongitude(String convertedLongitude) {
		this.convertedLongitude = convertedLongitude;
	}
}
