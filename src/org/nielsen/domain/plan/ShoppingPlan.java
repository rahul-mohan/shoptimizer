package org.nielsen.domain.plan;

import java.util.ArrayList;
import java.util.List;

import org.nielsen.service.composite.reponse.StoreLocationProductDetail;

public class ShoppingPlan {
	private String planName;
	private float totalCost;
	private float totalDistance;
	private int totalStores;
	private int totalAvailability;
	private List<StoreLocationProductDetail> stores;

	public List<StoreLocationProductDetail> getStores() {
		if (stores == null) {
			stores = new ArrayList<StoreLocationProductDetail>();
		}
		return stores;
	}

	public void setStores(List<StoreLocationProductDetail> stores) {
		this.stores = stores;
	}

	public float getTotalCost() {
		return totalCost;
	}

	public ShoppingPlan setTotalCost(float totalCost) {
		this.totalCost = totalCost;
		return this;
	}

	public float getTotalDistance() {
		return totalDistance;
	}

	public ShoppingPlan setTotalDistance(float totalDistance) {
		this.totalDistance = totalDistance;
		return this;
	}

	public int getTotalStores() {
		return totalStores;
	}

	public ShoppingPlan setTotalStores(int totalStores) {
		this.totalStores = totalStores;
		return this;
	}

	public int getTotalAvailability() {
		return totalAvailability;
	}

	public ShoppingPlan setTotalAvailability(int totalAvailability) {
		this.totalAvailability = totalAvailability;
		return this;
	}

	public String getPlanName() {
		return planName;
	}

	public ShoppingPlan setPlanName(String planName) {
		this.planName = planName;
		return this;
	}
}
