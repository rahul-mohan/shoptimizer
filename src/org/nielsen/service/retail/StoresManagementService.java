package org.nielsen.service.retail;

import java.util.HashMap;
import java.util.Map;

import org.apache.http.HttpHeaders;
import org.nielsen.http.util.HttpSecuredClientWrapper;
import org.nielsen.json.store.Store;
import org.nielsen.json.store.StoreCoordinate;
import org.nielsen.json.store.StoreRefData;
import org.nielsen.json.store.StoreReference;
import org.nielsen.web.util.ApiConstants;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;

import com.google.gson.Gson;

@Component
public class StoresManagementService {
	private static final String URL_STORE_SEARCH_BY_LAT_LON = "https://nielsen.api.tibco.com:443/Stores/v1/Location/?latitude=%s&longitude=%s&radius=5";
	private static final String URL_STORE_SEARCH_BY_ZIP_CODE = "https://nielsen.api.tibco.com:443/Stores/v1/Location/?zipcode=%s";
	private static final String URL_STORE_SEARCH_BY_STORE_CODE = "https://nielsen.api.tibco.com:443/Stores/v1/%s";

	public static void main(String[] args) {
		StoresManagementService service = new StoresManagementService();
		//System.out.println(service.retrieveStoresByGeoLocation("37.3158", "-79.9543"));
		//System.out.println(service.retrieveStoresByZipcode("61704"));
		System.out.println(service.retrieveSomeLatLongByZipCode("61704"));
	}
	
	public String retrieveStoresByGeoLocation(String latitude, String longitude) {
		HttpSecuredClientWrapper client = new  HttpSecuredClientWrapper();
		String url = String.format(URL_STORE_SEARCH_BY_LAT_LON, latitude, longitude);
		Map<String, String> headers = new HashMap<>();
		headers.put(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE);
		headers.put(ApiConstants.API_KEY, ApiConstants.API_KEY_VALUE);

		return client.execute(url, headers);
	}

	public StoreRefData retrieveStoreRefDataByGeoLocation(String latitude, String longitude) {
		String stores = retrieveStoresByGeoLocation(latitude, longitude);
		return StoreReference.getStoreRefDataFromJSONString(stores);
	}
	
	public String retrieveStoresByZipcode(String zipCode) {
		HttpSecuredClientWrapper client = new  HttpSecuredClientWrapper();
		String url = String.format(URL_STORE_SEARCH_BY_ZIP_CODE, zipCode);
		Map<String, String> headers = new HashMap<>();
		headers.put(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE);
		headers.put(ApiConstants.API_KEY, ApiConstants.API_KEY_VALUE);

		return client.execute(url, headers);
	}

	public String retrieveStoresByStoreCode(String storeCode) {
		HttpSecuredClientWrapper client = new  HttpSecuredClientWrapper();
		String url = String.format(URL_STORE_SEARCH_BY_STORE_CODE, storeCode);
		Map<String, String> headers = new HashMap<>();
		headers.put(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE);
		headers.put(ApiConstants.API_KEY, ApiConstants.API_KEY_VALUE);

		return client.execute(url, headers);
	}

	public StoreCoordinate retrieveSomeLatLongByZipCode(String zipCode) {
		String storeDetails = retrieveStoresByZipcode(zipCode);
		Gson gson = new Gson();
		StoreReference storeRef = gson.fromJson(storeDetails, StoreReference.class);	
		String storeCode = storeRef.retrieveAtLeastOneStoreCode();
		String storeInfo = retrieveStoresByStoreCode(storeCode);
		Store store = gson.fromJson(storeInfo, Store.class);
		return store.getStoreCoordinate();
	}
}
