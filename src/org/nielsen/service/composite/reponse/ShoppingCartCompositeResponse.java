package org.nielsen.service.composite.reponse;

import java.util.List;

import com.google.gson.Gson;

public class ShoppingCartCompositeResponse {
	private List<StoreLocationProductDetail> byCheapestPrice;
	private List<StoreLocationProductDetail> byAvailability;
	private List<StoreLocationProductDetail> byDistance;

	public List<StoreLocationProductDetail> getByCheapestPrice() {
		return byCheapestPrice;
	}
	public void setByCheapestPrice(List<StoreLocationProductDetail> byCheapestPrice) {
		this.byCheapestPrice = byCheapestPrice;
	}
	public List<StoreLocationProductDetail> getByAvailability() {
		return byAvailability;
	}
	public void setByAvailability(List<StoreLocationProductDetail> byAvailability) {
		this.byAvailability = byAvailability;
	}
	public List<StoreLocationProductDetail> getByDistance() {
		return byDistance;
	}
	public void setByDistance(List<StoreLocationProductDetail> byDistance) {
		this.byDistance = byDistance;
	}

	public String toJson() {
		return (new Gson()).toJson(this);
	}
}
