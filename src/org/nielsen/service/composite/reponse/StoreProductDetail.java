package org.nielsen.service.composite.reponse;

public class StoreProductDetail {
	private String upc;
	private String description;
	private String itemCode;
	private String module;
	private String rank;
	private float pricePerItem;
	private int quantity;
	private boolean available;

	public String getUpc() {
		return upc;
	}

	public void setUpc(String upc) {
		this.upc = upc;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getItemCode() {
		return itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	public String getModule() {
		return module;
	}

	public void setModule(String module) {
		this.module = module;
	}

	public String getRank() {
		return rank;
	}

	public void setRank(String rank) {
		this.rank = rank;
	}

	public float getPricePerItem() {
		return pricePerItem;
	}

	public void setPricePerItem(float pricePerItem) {
		this.pricePerItem = pricePerItem;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public boolean isAvailable() {
		return available;
	}

	public void setAvailable(boolean available) {
		this.available = available;
	}

	public float getTotalPrice() {
		return (float) quantity*pricePerItem;
	}
}
