package org.nielsen.service.composite.reponse;

import java.util.ArrayList;
import java.util.List;

public class StoreLocationProductDetail {
	private String storeId;
	private String storeNumber;
	private String storeCode;
	private String storeName;
	private String ownerName;
	private String storeStatus;
	private String phone;
	private String latitude;
	private String longitude;
	private String line1;
	private String city;
	private String state;
	private String zipCode;
	private String country;	
	private int requestedNumberOfProducts;
	private int availableNumberOfProducts;
	private double distanceFromRequestedLocation;
	private double totalPriceOfAvailableProducts;
	private List<StoreProductDetail> productDetails;

	public String getStoreId() {
		return storeId;
	}
	public void setStoreId(String storeId) {
		this.storeId = storeId;
	}
	public String getStoreNumber() {
		return storeNumber;
	}
	public void setStoreNumber(String storeNumber) {
		this.storeNumber = storeNumber;
	}
	public String getStoreCode() {
		return storeCode;
	}
	public void setStoreCode(String storeCode) {
		this.storeCode = storeCode;
	}
	public String getStoreName() {
		return storeName;
	}
	public void setStoreName(String storeName) {
		this.storeName = storeName;
	}
	public String getOwnerName() {
		return ownerName;
	}
	public void setOwnerName(String ownerName) {
		this.ownerName = ownerName;
	}
	public String getStoreStatus() {
		return storeStatus;
	}
	public void setStoreStatus(String storeStatus) {
		this.storeStatus = storeStatus;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getLatitude() {
		return latitude;
	}
	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}
	public String getLongitude() {
		return longitude;
	}
	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}
	public String getLine1() {
		return line1;
	}
	public void setLine1(String line1) {
		this.line1 = line1;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getZipCode() {
		return zipCode;
	}
	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public int getRequestedNumberOfProducts() {
		return requestedNumberOfProducts;
	}
	public void setRequestedNumberOfProducts(int requestedNumberOfProducts) {
		this.requestedNumberOfProducts = requestedNumberOfProducts;
	}
	public int getAvailableNumberOfProducts() {
		return availableNumberOfProducts;
	}
	public void setAvailableNumberOfProducts(int availableNumberOfProducts) {
		this.availableNumberOfProducts = availableNumberOfProducts;
	}
	public double getDistanceFromRequestedLocation() {
		return distanceFromRequestedLocation;
	}
	public void setDistanceFromRequestedLocation(double distanceFromRequestedLocation) {
		this.distanceFromRequestedLocation = distanceFromRequestedLocation;
	}
	public double getTotalPriceOfAvailableProducts() {
		return totalPriceOfAvailableProducts;
	}
	public void setTotalPriceOfAvailableProducts(double totalPriceOfAvailableProducts) {
		this.totalPriceOfAvailableProducts = totalPriceOfAvailableProducts;
	}
	public List<StoreProductDetail> getProductDetails() {
		if (productDetails == null) {
			productDetails = new ArrayList<StoreProductDetail>();
		}
		return productDetails;
	}
	public void setProductDetails(List<StoreProductDetail> productDetails) {
		this.productDetails = productDetails;
	}
}
