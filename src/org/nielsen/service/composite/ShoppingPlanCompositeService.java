package org.nielsen.service.composite;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.nielsen.domain.plan.ShoppingPlan;
import org.nielsen.domain.plan.ShoppingPlans;
import org.nielsen.json.product.ProductAvailabilityDetail;
import org.nielsen.json.product.Retailer;
import org.nielsen.json.product.StoreDistance;
import org.nielsen.json.product.StorePrice;
import org.nielsen.json.store.StoreCoordinate;
import org.nielsen.service.composite.reponse.StoreLocationProductDetail;
import org.nielsen.service.composite.reponse.StoreProductDetail;
import org.nielsen.service.composite.request.ShoppingCartCompositeRequest;
import org.nielsen.service.composite.request.ShoppingCartItem;
import org.nielsen.service.product.ProductAvailabilityManagementService;
import org.nielsen.service.retail.StoresManagementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import com.google.gson.Gson;

@Component
public class ShoppingPlanCompositeService {

	@Autowired
	private StoresManagementService smService;

	@Autowired
	private ProductAvailabilityManagementService pamService;

	public static void main(String[] args) {
		ShoppingPlanCompositeService spcService = new ShoppingPlanCompositeService();
		ShoppingCartCompositeRequest request = new ShoppingCartCompositeRequest();
		request.setZipCode("77015");
		ShoppingCartItem item1 = new ShoppingCartItem("0016000275270", 9);
		ShoppingCartItem item2 = new ShoppingCartItem("0016000275271", 1);
		request.getItems().add(item1);
		request.getItems().add(item2);
		System.out.println((new Gson()).toJson(spcService.createShoppingPlans(request)));
	}

	public ShoppingPlans createShoppingPlans(ShoppingCartCompositeRequest request) {
		ShoppingPlans plans = null;
		try {
			plans = buildShoppingPlans(request);
			plans.setSuccess(true);
		} catch(Throwable t) {
			plans = new ShoppingPlans();
			plans.setMessage(t.getMessage());
		}
		
		return plans;
	}

	private ShoppingPlans buildShoppingPlans(ShoppingCartCompositeRequest request) {
		validateShoppingCartCompositeRequest(request);
		List<ProductAvailabilityDetail> productsAvailability = null;
		ShoppingPlans plans = new ShoppingPlans();

		// Zipcode takes precedence over GeoLocation
		if (StringUtils.hasText(request.getZipCode())) {
			StoreCoordinate storeCoordinate = getSmService().retrieveSomeLatLongByZipCode(request.getZipCode());
			String latitude = storeCoordinate.getLatitude();
			String longitude = storeCoordinate.getLongitude();
			validateLatAndLong(latitude, longitude);
			plans.setConvertedLatitude(latitude);
			plans.setConvertedLongitude(longitude);
			productsAvailability = getPamService().retrieveProductAvailabilityDetailsAsync(request.getUpcCodes(), latitude,
					longitude, request.getSearchRadius());
		} else {
			validateLatAndLong(request.getLatitude(), request.getLongitude());
			productsAvailability = getPamService().retrieveProductAvailabilityDetails(request.getUpcCodes(),
					request.getLatitude(), request.getLongitude(), request.getSearchRadius());
		}
		// UPC Code, Store, Price
		Map<String, StorePrice> cheapestPrice = new HashMap<String, StorePrice>();
		// UPC Code, Store, Distance
		Map<String, StoreDistance> shortestDistance = new HashMap<String, StoreDistance>();
		populateCheapestPriceAndCheapestDistanceMaps(productsAvailability, cheapestPrice, shortestDistance);

		ShoppingPlan cheapestPricePlan = buildCheapestPricePlan(productsAvailability, request.getItems(), cheapestPrice);
		plans.addPlan(cheapestPricePlan);
		ShoppingPlan shortestDistancePlan = buildShortestDistancePlan(productsAvailability, request.getItems(),
				shortestDistance);
		plans.addPlan(shortestDistancePlan);
		return plans;
	}

	private ShoppingPlan buildCheapestPricePlan(List<ProductAvailabilityDetail> productsAvailability,
			List<ShoppingCartItem> items, Map<String, StorePrice> cheapestPrice) {
		ShoppingPlan plan = new ShoppingPlan();
		plan.setPlanName("Cheapest Price");
		if (CollectionUtils.isNotEmpty(productsAvailability) && MapUtils.isNotEmpty(cheapestPrice)) {
			Map<String, String> barCodeToStoreIds = extractBarCodeToStoreIdFromStorePrice(cheapestPrice);
			Map<String, Integer> barCodeToQuantities = extractBarCodeToQuantity(items);
			plan.getStores().addAll(
					buildStoreLocationProductDetails(productsAvailability, barCodeToStoreIds, barCodeToQuantities));
			populatePlanSummary(plan);
		}
		return plan;
	}

	private void populatePlanSummary(ShoppingPlan plan) {
		float totalCost = 0.0f;
		float totalDistance = 0.0f;
		int totalAvailability = 0;

		if (CollectionUtils.isNotEmpty(plan.getStores())) {
			plan.setTotalStores(plan.getStores().size());
			for (StoreLocationProductDetail detail : plan.getStores()) {
				totalCost += detail.getTotalPriceOfAvailableProducts();
				totalDistance += detail.getDistanceFromRequestedLocation();
				totalAvailability += detail.getAvailableNumberOfProducts();
			}
		}
		plan.setTotalCost(totalCost);
		plan.setTotalDistance(totalDistance);
		plan.setTotalAvailability(totalAvailability);
	}

	private ShoppingPlan buildShortestDistancePlan(List<ProductAvailabilityDetail> productsAvailability,
			List<ShoppingCartItem> items, Map<String, StoreDistance> shortestDistance) {
		ShoppingPlan plan = new ShoppingPlan();
		plan.setPlanName("Shortest Distance");
		if (CollectionUtils.isNotEmpty(productsAvailability) && MapUtils.isNotEmpty(shortestDistance)) {
			Map<String, String> barCodeToStoreIds = extractBarCodeToStoreIdFromStoreDisatnce(shortestDistance);
			Map<String, Integer> barCodeToQuantities = extractBarCodeToQuantity(items);
			plan.getStores().addAll(
					buildStoreLocationProductDetails(productsAvailability, barCodeToStoreIds, barCodeToQuantities));
			populatePlanSummary(plan);
		}
		return plan;
	}

	private List<StoreLocationProductDetail> buildStoreLocationProductDetails(
			List<ProductAvailabilityDetail> productsAvailability, Map<String, String> barCodeToStoreIds,
			Map<String, Integer> barCodeToQuantities) {
		List<StoreLocationProductDetail> details = new ArrayList<StoreLocationProductDetail>();
		for (Map.Entry<String, String> barCodeToStoreId : barCodeToStoreIds.entrySet()) {
			StoreLocationProductDetail detail = new StoreLocationProductDetail();
			detail.setRequestedNumberOfProducts(barCodeToQuantities.size());
			populateStoreLocationProductDetail(productsAvailability, barCodeToStoreId.getKey(),
					barCodeToStoreId.getValue(), barCodeToQuantities, detail);
			details.add(detail);
		}

		return details;
	}

	private void populateStoreLocationProductDetail(List<ProductAvailabilityDetail> productsAvailability,
			String upcCode, String storeId, Map<String, Integer> barCodeToQuantities, StoreLocationProductDetail detail) {
		int availableNumberOfProducts = 0;
		if (CollectionUtils.isNotEmpty(productsAvailability)) {
			for (ProductAvailabilityDetail availability : productsAvailability) {
				if (availability != null) {
					Retailer retailer = availability.retrieveRetailer(upcCode, storeId);
					if (retailer != null) {
						availableNumberOfProducts += 1;
						populateStoreLocationDetail(availability, retailer, detail);
						populateProductDetail(availability, retailer, barCodeToQuantities, detail);
					}
				}
			}
		}
		detail.setAvailableNumberOfProducts(availableNumberOfProducts);
	}

	private void populateProductDetail(ProductAvailabilityDetail availability, Retailer retailer,
			Map<String, Integer> barCodeToQuantities, StoreLocationProductDetail detail) {
		StoreProductDetail productDetail = new StoreProductDetail();
		productDetail.setPricePerItem(retailer.getPrice());
		productDetail.setQuantity(barCodeToQuantities.get(availability.getBarCode()));
		productDetail.setAvailable(true);
		productDetail.setDescription(availability.getProductName());
		productDetail.setUpc(availability.getBarCode());
		double totalPrice = (double) (detail.getTotalPriceOfAvailableProducts() + retailer.getPrice()
				* productDetail.getQuantity());
		detail.setTotalPriceOfAvailableProducts(Double.valueOf(String.format("%.2f", totalPrice)));
		detail.getProductDetails().add(productDetail);
	}

	private void populateStoreLocationDetail(ProductAvailabilityDetail availability, Retailer retailer,
			StoreLocationProductDetail detail) {
		detail.setStoreId(retailer.getStoreId());
		detail.setStoreName(retailer.getStoreName());
		detail.setOwnerName(retailer.getRetailerName());
		detail.setStoreNumber(retailer.getStoreNumber());
		detail.setDistanceFromRequestedLocation(Double.valueOf(retailer.getDistance()));
		detail.setLatitude(retailer.getRetailerLocation().getLatitude());
		detail.setLongitude(retailer.getRetailerLocation().getLongitude());
		detail.setPhone(retailer.getRetailerLocation().getPhone());
		detail.setLine1(retailer.getRetailerLocation().getLine1());
		detail.setCity(retailer.getRetailerLocation().getCity());
		detail.setState(retailer.getRetailerLocation().getState());
		detail.setZipCode(retailer.getRetailerLocation().getZipCode());
		detail.setCountry(retailer.getRetailerLocation().getCountry());
	}

	private Map<String, Integer> extractBarCodeToQuantity(List<ShoppingCartItem> items) {
		Map<String, Integer> barCodeToQuantities = new HashMap<String, Integer>();
		if (CollectionUtils.isNotEmpty(items)) {
			for (ShoppingCartItem item : items) {
				if (item != null) {
					barCodeToQuantities.put(item.getUpcCode(), item.getQuantity());
				}
			}
		}
		return barCodeToQuantities;
	}

	private Map<String, String> extractBarCodeToStoreIdFromStorePrice(Map<String, StorePrice> cheapestPrice) {
		Map<String, String> barCodeToStoreIds = new HashMap<String, String>();
		for (Map.Entry<String, StorePrice> price : cheapestPrice.entrySet()) {
			barCodeToStoreIds.put(price.getKey(), price.getValue().getStoreId());
		}
		return barCodeToStoreIds;
	}

	private Map<String, String> extractBarCodeToStoreIdFromStoreDisatnce(Map<String, StoreDistance> shortestDistance) {
		Map<String, String> barCodeToStoreIds = new HashMap<String, String>();
		for (Map.Entry<String, StoreDistance> distance : shortestDistance.entrySet()) {
			barCodeToStoreIds.put(distance.getKey(), distance.getValue().getStoreId());
		}
		return barCodeToStoreIds;
	}

	private void populateCheapestPriceAndCheapestDistanceMaps(List<ProductAvailabilityDetail> productsAvailability,
			Map<String, StorePrice> cheapestPrice, Map<String, StoreDistance> shortestDistance) {
		if (CollectionUtils.isNotEmpty(productsAvailability)) {
			for (ProductAvailabilityDetail detail : productsAvailability) {
				populateCheapestPriceAndCheapestDistanceMaps(detail, cheapestPrice, shortestDistance);
			}
		}
	}

	private void populateCheapestPriceAndCheapestDistanceMaps(ProductAvailabilityDetail detail,
			Map<String, StorePrice> cheapestPrice, Map<String, StoreDistance> shortestDistance) {
		if (detail != null) {
			List<Retailer> allRetailers = detail.retrieveAllRetailers();
			if (CollectionUtils.isNotEmpty(allRetailers)) {
				for (Retailer retailer : allRetailers) {
					populateCheapestPriceAndCheapestDistanceMaps(retailer, detail.getBarCode(), cheapestPrice,
							shortestDistance);
				}
			}
		}
	}

	private void populateCheapestPriceAndCheapestDistanceMaps(Retailer retailer, String barCode,
			Map<String, StorePrice> cheapestPrice, Map<String, StoreDistance> shortestDistance) {
		if (retailer != null) {
			StorePrice price = cheapestPrice.get(barCode);
			if (price == null) {
				cheapestPrice.put(barCode, new StorePrice(retailer.getStoreId(), retailer.getPrice()));
			} else if (price.getPrice() > retailer.getPrice()) {
				cheapestPrice.put(barCode, price.setStoreIdAndPrice(retailer.getStoreId(), retailer.getPrice()));
			} else if (price.getPrice() == retailer.getPrice()) {
				// Prices are equal select the shortest distance
				StoreDistance distance = shortestDistance.get(barCode);
				if (retailer.getDistanceAsFloat() < distance.getDistance()) {
					cheapestPrice.put(barCode, price.setStoreIdAndPrice(retailer.getStoreId(), retailer.getPrice()));
				}
			}

			StoreDistance distance = shortestDistance.get(barCode);
			if (distance == null) {
				shortestDistance.put(barCode, new StoreDistance(retailer.getStoreId(), retailer.getDistanceAsFloat()));
			} else if (distance.getDistance() > retailer.getDistanceAsFloat()) {
				shortestDistance.put(barCode,
						distance.setStoreIdAndDistance(retailer.getStoreId(), retailer.getDistanceAsFloat()));
			} else if (distance.getDistance() == retailer.getDistanceAsFloat()) {
				// Distance are equal select the cheapest price
				StorePrice currentPrice = cheapestPrice.get(barCode);
				if (currentPrice.getPrice() > retailer.getPrice()) {
					shortestDistance.put(barCode,
							distance.setStoreIdAndDistance(retailer.getStoreId(), retailer.getDistanceAsFloat()));
				}
			}
		}
	}

	private void validateLatAndLong(String latitude, String longitude) {
		StringBuilder errorMessages = new StringBuilder();
		if (!StringUtils.hasText(latitude) || !StringUtils.hasText(longitude)) {
			errorMessages.append("The latitude and longitude cannot be null or empty");
		}

		if (StringUtils.hasText(errorMessages.toString())) {
			errorMessages.append(String.format("The provided input : lat[%s], long[%s]", latitude, longitude));
			throw new RuntimeException(errorMessages.toString());
		}
	}

	private void validateShoppingCartCompositeRequest(ShoppingCartCompositeRequest request) {
		StringBuilder errorMessages = new StringBuilder();
		if (request == null) {
			errorMessages.append("Shopping cart composite request cannot be null");
		} else if (CollectionUtils.isEmpty(request.getItems())) {
			errorMessages.append("Shopping cart items cannot be null or empty");
		} else if (request.getLatitude() == null && request.getLongitude() == null && request.getZipCode() == null) {
			errorMessages.append("Both Geolocation and ZipCode cannot be null. One is must.");
		}

		if (StringUtils.hasText(errorMessages.toString())) {
			errorMessages.append("The provided input : ");
			errorMessages.append((new Gson()).toJson(request));
			throw new RuntimeException(errorMessages.toString());
		}
	}

	public StoresManagementService getSmService() {
		if (smService == null) {
			// For non spring hookup
			smService = new StoresManagementService();
		}
		return smService;
	}

	public ProductAvailabilityManagementService getPamService() {
		if (pamService == null) {
			// For non spring hookup
			pamService = new ProductAvailabilityManagementService();
		}
		return pamService;
	}
}
