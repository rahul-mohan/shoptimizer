package org.nielsen.service.composite;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.nielsen.json.product.ProductAvailabilityDetail;
import org.nielsen.json.product.ProductDetail;
import org.nielsen.json.product.Retailer;
import org.nielsen.json.store.StoreCoordinate;
import org.nielsen.service.composite.reponse.ShoppingCartCompositeResponse;
import org.nielsen.service.composite.reponse.StoreLocationProductDetail;
import org.nielsen.service.composite.reponse.StoreProductDetail;
import org.nielsen.service.product.ProductAvailabilityManagementService;
import org.nielsen.service.product.ProductReferenceManagementService;
import org.nielsen.service.retail.StoresManagementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.gson.Gson;

@Component
public class LocalStoreProductCompositeService {
	@Autowired
	private StoresManagementService smService;

	@Autowired
	private ProductReferenceManagementService prmService;

	@Autowired
	private ProductAvailabilityManagementService pamService;

	public static void main(String[] args) {
		LocalStoreProductCompositeService service = new LocalStoreProductCompositeService();
		// List<String> productNames = new ArrayList<String>();
		// productNames.add("CEREAL-READY TO EAT");
		// productNames.add("SPORT DRINK");
		// productNames.add("WATER-BOTTLED-NONCARBONATED");
		// productNames.add("FACIAL TISSUE");
		// productNames.add("HAND & BODY LOTION-ADULT");
		// System.out.println((new
		// Gson()).toJson(service.retrieveSortedLocalStoreProductsByName(productNames,
		// "29.8526", "-95.199")));
		List<String> upcCodes = new ArrayList<String>();
		upcCodes.add("0016000275270");
		upcCodes.add("0050200957222");
		// System.out.println((new
		// Gson()).toJson(service.retrieveSortedLocalStoreProductsByUpcCodes(upcCodes,
		// "29.8526","-95.199")));
		System.out.println((new Gson()).toJson(service.retrieveSortedLocalStoreProductsByUpcCodes(upcCodes, "77015", 15)));

	}

	public ShoppingCartCompositeResponse retrieveSortedLocalStoreProductsByName(List<String> productNames,
			String latitude, String longitude, int radius) {
		List<ProductDetail> products = getPrmService().retrieveProductDetailsByNames(productNames);
		Set<String> uniqueUpcCodes = retrieveUniqueUPCCodes(products);
		List<ProductAvailabilityDetail> productsAvailability = getPamService().retrieveProductAvailabilityDetails(
				new ArrayList<String>(uniqueUpcCodes), latitude, longitude, radius);
		return sort(productsAvailability, uniqueUpcCodes, latitude, longitude);
	}

	public ShoppingCartCompositeResponse retrieveSortedLocalStoreProductsByUpcCodes(List<String> upcCodes,
			String latitude, String longitude, int radius) {
		List<ProductAvailabilityDetail> productsAvailability = getPamService().retrieveProductAvailabilityDetails(
				upcCodes, latitude, longitude, radius);
		return sort(productsAvailability, new HashSet<String>(upcCodes), latitude, longitude);
	}

	public ShoppingCartCompositeResponse retrieveSortedLocalStoreProductsByUpcCodes(List<String> upcCodes,
			String zipCode, int radius) {
		StoreCoordinate storeCoordinate = getSmService().retrieveSomeLatLongByZipCode(zipCode);
		String latitude = storeCoordinate.getLatitude();
		String longitude = storeCoordinate.getLongitude();
		List<ProductAvailabilityDetail> productsAvailability = getPamService().retrieveProductAvailabilityDetails(
				upcCodes, latitude, longitude, radius);
		return sort(productsAvailability, new HashSet<String>(upcCodes), latitude, longitude);
	}

	private ShoppingCartCompositeResponse sort(List<ProductAvailabilityDetail> productsAvailability,
			Set<String> upcCodes, String latitude, String longitude) {
		ShoppingCartCompositeResponse response = new ShoppingCartCompositeResponse();
		response.setByAvailability(buildAndSortByAvailability(productsAvailability, upcCodes, latitude, longitude));
		return response;
	}

	private List<StoreLocationProductDetail> buildAndSortByAvailability(
			List<ProductAvailabilityDetail> productsAvailability, Set<String> upcCodes, String latitude,
			String longitude) {
		List<StoreLocationProductDetail> details = new ArrayList<StoreLocationProductDetail>();
		Set<String> uniqueStoreIds = ProductAvailabilityDetail.buildUniqueStoresById(productsAvailability);
		if (CollectionUtils.isNotEmpty(uniqueStoreIds)) {
			for (String storeId : uniqueStoreIds) {
				details.add(buildStoreLocationProductDetail(productsAvailability, upcCodes, storeId));
			}
		}
		return details;
	}

	private StoreLocationProductDetail buildStoreLocationProductDetail(
			List<ProductAvailabilityDetail> productsAvailability, Set<String> upcCodes, String storeId) {
		StoreLocationProductDetail detail = new StoreLocationProductDetail();
		if (CollectionUtils.isNotEmpty(upcCodes)) {
			detail.setRequestedNumberOfProducts(upcCodes.size());
			for (String upcCode : upcCodes) {
				populateStoreLocationProductDetail(productsAvailability, upcCode, storeId, detail);
			}
		}
		return detail;
	}

	private void populateStoreLocationProductDetail(List<ProductAvailabilityDetail> productsAvailability,
			String upcCode, String storeId, StoreLocationProductDetail detail) {
		if (CollectionUtils.isNotEmpty(productsAvailability)) {
			for (ProductAvailabilityDetail availability : productsAvailability) {
				if (availability != null) {
					Retailer retailer = availability.retrieveRetailer(upcCode, storeId);
					if (retailer != null) {
						populateStoreLocationDetail(availability, retailer, detail);
						populateProductDetail(availability, detail);
						detail.setAvailableNumberOfProducts(detail.getAvailableNumberOfProducts() + 1);
					}
				}
			}
		}
	}

	private void populateProductDetail(ProductAvailabilityDetail availability, StoreLocationProductDetail detail) {
		StoreProductDetail productDetail = new StoreProductDetail();
		productDetail.setAvailable(true);
		productDetail.setDescription(availability.getProductName());
		productDetail.setUpc(availability.getBarCode());
		detail.getProductDetails().add(productDetail);
	}

	private void populateStoreLocationDetail(ProductAvailabilityDetail availability, Retailer retailer,
			StoreLocationProductDetail detail) {
		detail.setStoreId(retailer.getStoreId());
		detail.setStoreName(retailer.getStoreName());
		detail.setOwnerName(retailer.getRetailerName());
		detail.setStoreNumber(retailer.getStoreNumber());
		detail.setDistanceFromRequestedLocation(Double.valueOf(retailer.getDistance()));
		detail.setLatitude(retailer.getRetailerLocation().getLatitude());
		detail.setLongitude(retailer.getRetailerLocation().getLongitude());
		detail.setPhone(retailer.getRetailerLocation().getPhone());
		detail.setLine1(retailer.getRetailerLocation().getLine1());
		detail.setCity(retailer.getRetailerLocation().getCity());
		detail.setState(retailer.getRetailerLocation().getState());
		detail.setZipCode(retailer.getRetailerLocation().getZipCode());
		detail.setCountry(retailer.getRetailerLocation().getCountry());
	}

	private Set<String> retrieveUniqueUPCCodes(List<ProductDetail> products) {
		Set<String> upcCodes = new HashSet<String>();
		if (CollectionUtils.isNotEmpty(products)) {
			for (ProductDetail product : products) {
				upcCodes.add(product.getUPC());
			}
		}
		return upcCodes;
	}

	public StoresManagementService getSmService() {
		if (smService == null) {
			// For non spring hookup
			smService = new StoresManagementService();
		}
		return smService;
	}

	public ProductReferenceManagementService getPrmService() {
		if (prmService == null) {
			// For non spring hookup
			prmService = new ProductReferenceManagementService();
		}
		return prmService;
	}

	public ProductAvailabilityManagementService getPamService() {
		if (pamService == null) {
			// For non spring hookup
			pamService = new ProductAvailabilityManagementService();
		}
		return pamService;
	}
}
