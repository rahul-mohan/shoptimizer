package org.nielsen.service.composite.request;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import com.google.gson.Gson;

public class ShoppingCartCompositeRequest implements Serializable {
	private static final long serialVersionUID = 1L;
	private List<ShoppingCartItem> items;
	private String latitude;
	private String longitude;
	private String zipCode;
	private int searchRadius;

	public static void main(String[] args) {
		Scanner fileScanner = null;
		try {
			fileScanner = new Scanner(new File("ShoppingCartCompositeRequest"));
			String storeDetails = fileScanner.useDelimiter("\\Z").next();
			Gson gson = new Gson();
			ShoppingCartCompositeRequest obj = gson.fromJson(storeDetails, ShoppingCartCompositeRequest.class);
			System.out.println(gson.toJson(obj));
		} catch (FileNotFoundException e) {
			throw new RuntimeException(e.getMessage());
		} finally {
			if (fileScanner != null) {
				fileScanner.close();
			}
		}
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public List<ShoppingCartItem> getItems() {
		if (items == null) {
			items = new ArrayList<ShoppingCartItem>();
		}
		return items;
	}

	public void setItems(List<ShoppingCartItem> items) {
		this.items = items;
	}

	public List<String> getUpcCodes() {
		List<String> upcCodes = new ArrayList<String>();
		if (items != null && items.size() > 0) {
			for (ShoppingCartItem item : items) {
				upcCodes.add(item.getUpcCode());
			}
		}
		return upcCodes;
	}

	public int getSearchRadius() {
		return searchRadius;
	}

	public void setSearchRadius(int searchRadius) {
		this.searchRadius = searchRadius;
	}
}
