package org.nielsen.service.composite.request;

import java.io.Serializable;

public class ShoppingCartItem implements Serializable {
	private static final long serialVersionUID = 1L;
	private String upcCode;
	private int quantity;

	public ShoppingCartItem(String upcCode, int quantity) {
		this.upcCode = upcCode;
		this.quantity = quantity;
	}

	public String getUpcCode() {
		return upcCode;
	}

	public void setUpcCode(String upcCode) {
		this.upcCode = upcCode;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

}
