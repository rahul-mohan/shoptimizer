package org.nielsen.service.geocoding;

import java.util.HashMap;
import java.util.Map;

import org.apache.http.HttpHeaders;
import org.nielsen.http.util.HttpSecuredClientWrapper;
import org.springframework.http.MediaType;

public class GeocodingUtil {
	private static final String URL_ZIPCODE_SEARCH_BY_LAT_LON = "https://maps.googleapis.com/maps/api/geocode/json?latlng=%s,%s&sensor=false&api=%s";
	private static final String API_KEY = "AIzaSyBlHO58pS_5hg9TyriCcffGp8rTuJZ_JYA";

	public static void main(String[] args) {
		GeocodingUtil util = new GeocodingUtil();
		System.out.println(util.retrieveZipCodeByGeoLocation("40.714224", "-73.961452"));
	}

	public String retrieveZipCodeByGeoLocation(String latitude, String longitude) {
		HttpSecuredClientWrapper client = new  HttpSecuredClientWrapper();
		String url = String.format(URL_ZIPCODE_SEARCH_BY_LAT_LON, latitude, longitude, API_KEY);
		Map<String, String> headers = new HashMap<>();
		headers.put(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE);

		return client.execute(url, headers);
	}	
}
