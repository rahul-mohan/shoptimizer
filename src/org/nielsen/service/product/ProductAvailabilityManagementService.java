package org.nielsen.service.product;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import org.apache.http.HttpHeaders;
import org.nielsen.http.util.HttpSecuredClientWrapper;
import org.nielsen.json.product.AvailableStore;
import org.nielsen.json.product.ProductAvailability;
import org.nielsen.json.product.ProductAvailabilityDetail;
import org.nielsen.json.product.Retailer;
import org.nielsen.web.util.ApiConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import com.google.gson.Gson;

@Component
public class ProductAvailabilityManagementService {
	private static final String URL_PRODUCT_AVAILABILITY_BY_UPC_AND_LOC = "https://nielsen.api.tibco.com:443/StoreAvailability/v1?product_id=%s&lat=%s&long=%s&distance=%s";
	private static final int DEFAULT_RADIUS = 5;

	@Autowired
	private ProductPricingManagementService ppmService;

	public static void main(String[] args) {
		ProductAvailabilityManagementService pamService = new ProductAvailabilityManagementService();
		List<String> upcCodes = new ArrayList<String>();
		upcCodes.add("0016000275270");
		upcCodes.add("0042400036357");
		System.out.println((new Gson()).toJson(pamService.retrieveProductAvailabilityDetailsAsync(upcCodes, "29.7904",
				"-95.1624", 20)));
	}

	public ProductPricingManagementService getPpmService() {
		if (ppmService == null) {
			ppmService = new ProductPricingManagementService();
		}
		return ppmService;
	}

	public List<ProductAvailabilityDetail> retrieveProductAvailabilityDetailsAsync(List<String> codes, String latitude,
			String longitude, int radius) {
		List<ProductAvailabilityDetail> availabilityDetails = new ArrayList<ProductAvailabilityDetail>();
		List<Future<ProductAvailabilityDetail>> futureDetails = new ArrayList<Future<ProductAvailabilityDetail>>();
		
		if (radius <= 0) {
			radius = DEFAULT_RADIUS;
		}
		if (codes != null && codes.size() > 0) {
			for (String code : codes) {
				System.out.println(String.format("Processing [%s][%s][%s][%s]", code, latitude, longitude, radius));
				Future<ProductAvailabilityDetail> future = retrieveProductAvailabilityDetailsAsync(code, latitude,
						longitude, radius);
				futureDetails.add(future);
			}
		}

		for (Future<ProductAvailabilityDetail> future : futureDetails) {
			try {
				ProductAvailabilityDetail availability = future.get();
				if (availability != null && StringUtils.hasText(availability.getBarCode())) {
					availabilityDetails.add(availability);
				}
			} catch (InterruptedException e) {
				// During errors log, ignore and continue
				e.printStackTrace();
			} catch (ExecutionException e) {
				// During errors log, ignore and continue
				e.printStackTrace();
			}
		}

		populateProductPricingPerRetailer(availabilityDetails);
		return availabilityDetails;
	}

	public List<ProductAvailabilityDetail> retrieveProductAvailabilityDetails(List<String> codes, String latitude,
			String longitude, int radius) {
		List<ProductAvailabilityDetail> availabilityDetails = new ArrayList<ProductAvailabilityDetail>();
		if (radius <= 0) {
			radius = DEFAULT_RADIUS;
		}		
		if (codes != null && codes.size() > 0) {
			for (String code : codes) {
				System.out.println(String.format("Processing [%s][%s][%s][%s]", code, latitude, longitude, radius));
				String availability = retrieveProductAvailabilityDetails(code, latitude, longitude, radius);
				availabilityDetails.add(ProductAvailability.retrieveProductAvailabilityDetails(availability));
			}
		}
		populateProductPricingPerRetailer(availabilityDetails);
		return availabilityDetails;
	}

	private String retrieveProductAvailabilityDetails(String code, String latitude, String longitude, int radius) {
		HttpSecuredClientWrapper client = new HttpSecuredClientWrapper();
		String url = String.format(URL_PRODUCT_AVAILABILITY_BY_UPC_AND_LOC, code, latitude, longitude, radius);
		Map<String, String> headers = new HashMap<>();
		headers.put(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE);
		headers.put(ApiConstants.API_KEY, ApiConstants.API_KEY_VALUE);

		return client.execute(url, headers);
	}

	@Async
	private Future<ProductAvailabilityDetail> retrieveProductAvailabilityDetailsAsync(String code, String latitude,
			String longitude, int radius) {
		String availability = retrieveProductAvailabilityDetails(code, latitude, longitude, radius);
		return new AsyncResult<>(ProductAvailability.retrieveProductAvailabilityDetails(availability));
	}

	private void populateProductPricingPerRetailer(List<ProductAvailabilityDetail> details) {
		if (details != null && details.size() > 0) {
			for (ProductAvailabilityDetail detail : details) {
				populateProductPricingPerRetailer(detail);
			}
		}
	}

	private void populateProductPricingPerRetailer(ProductAvailabilityDetail detail) {
		if (detail != null && detail.getAvailabileStores() != null && detail.getAvailabileStores().size() > 0) {
			List<AvailableStore> stores = detail.getAvailabileStores();
			for (AvailableStore store : stores) {
				populateProductPricingPerRetailer(store, detail.getBarCode());
			}
		}
	}

	private void populateProductPricingPerRetailer(AvailableStore store, String barCode) {
		if (store != null && store.getRetailers() != null && store.getRetailers().size() > 0) {
			List<Retailer> retailers = store.getRetailers();
			for (Retailer retailer : retailers) {
				populateProductPricingPerRetailer(retailer, barCode);
			}
		}
	}

	private void populateProductPricingPerRetailer(Retailer retailer, String barCode) {
		if (retailer != null) {
			retailer.setPrice(getPpmService().retrieveProductPriceByUpcCode(barCode));
		}
	}
}
