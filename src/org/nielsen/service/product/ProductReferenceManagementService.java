package org.nielsen.service.product;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.http.HttpHeaders;
import org.nielsen.http.util.HttpSecuredClientWrapper;
import org.nielsen.json.product.ProductDetail;
import org.nielsen.json.product.ProductDetails;
import org.nielsen.web.util.ApiConstants;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;

@Component
public class ProductReferenceManagementService {
	private static final String URL_PRODUCT_SEARCH_BY_CODE = "https://nielsen.api.tibco.com:443/Products/v1/?search=%s";
	private static final String URL_PRODUCT_SEARCH_BY_NAME = "https://nielsen.api.tibco.com:443/Products/v1/?search=%s";

	public static void main(String[] args) {
		ProductReferenceManagementService pmService = new ProductReferenceManagementService();
		System.out.println(pmService.retrieveProductDetailsByCode("0072151461207"));
		System.out.println(pmService.retrieveProductDetailsByName("CEREAL-READY TO EAT"));
	}

	public String retrieveProductDetailsByCode(String byCode) {
		validateProductCode(byCode);
		HttpSecuredClientWrapper client = new  HttpSecuredClientWrapper();
		String url = String.format(URL_PRODUCT_SEARCH_BY_CODE, byCode);
		Map<String, String> headers = new HashMap<>();
		headers.put(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE);
		headers.put(ApiConstants.API_KEY, ApiConstants.API_KEY_VALUE);

		return client.execute(url, headers);
	}

	public String retrieveProductDetailsByName(String productName) {
		validateProductName(productName);
		HttpSecuredClientWrapper client = new  HttpSecuredClientWrapper();
		String url = String.format(URL_PRODUCT_SEARCH_BY_NAME, client.urlParameterEncode(productName));
		Map<String, String> headers = new HashMap<>();
		headers.put(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE);
		headers.put(ApiConstants.API_KEY, ApiConstants.API_KEY_VALUE);

		return client.execute(url, headers);
	}	

	public List<ProductDetail> retrieveProductDetailsByNames(List<String> productNames) {
		List<ProductDetail> productDetails = new ArrayList<ProductDetail>();
		if (productNames != null && productNames.size() > 0) {
			for (String productName : productNames) {
				String productDetail = retrieveProductDetailsByName(productName);
				List<ProductDetail> products = ProductDetails.getProductDetailsFromJSONString(productDetail);
				productDetails.addAll(products);
			}
		}
		return productDetails;
	}

	private void validateProductCode(String code) {
		if (code == null || code.length() == 0) {
			throw new RuntimeException("Product Code cannot be null or empty");
		}
	}

	private void validateProductName(String name) {
		if (name == null || name.length() == 0) {
			throw new RuntimeException("Product Name cannot be null or empty");
		}
	}
}
