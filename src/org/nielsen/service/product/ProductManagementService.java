package org.nielsen.service.product;

import java.util.HashMap;
import java.util.Map;

import org.apache.http.HttpHeaders;
import org.nielsen.http.util.HttpSecuredClientWrapper;
import org.nielsen.web.util.ApiConstants;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;

@Component
public class ProductManagementService {
	private static final String URL_PRODUCT_SEARCH_BY_CODE = "https://nielsen.api.tibco.com:443/Products/v1/?search=%s";

	public static void main(String[] args) {
		ProductManagementService pmService = new ProductManagementService();
		System.out.println(pmService.retrieveProductDetails("0072151461207"));
	}

	String retrieveProductDetails(String byCode) {
		validateProductCode(byCode);
		HttpSecuredClientWrapper client = new HttpSecuredClientWrapper();
		String url = String.format(URL_PRODUCT_SEARCH_BY_CODE, byCode);
		Map<String, String> headers = new HashMap<>();
		headers.put(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE);
		headers.put(ApiConstants.API_KEY, ApiConstants.API_KEY_VALUE);		

		return client.execute(url, headers);		
	}

	private void validateProductCode(String code) {
		if (code == null || code.length() == 0) {
			throw new RuntimeException("Product Code cannot be null or empty");
		}
	}
}
