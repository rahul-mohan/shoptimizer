package org.nielsen.service.product;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.http.HttpHeaders;
import org.nielsen.http.util.HttpSecuredClientWrapper;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;

@Component
public class ProductPricingManagementService {
	private static final String URL_PRODUCT_PRICING_BY_UPC = "https://api.priceapi.com/products/single?token=QGRKRKHSZOHLJNNQKNQERZJAVNGDKCQKAABKYFMNAUXZSJOTNWMPAMYXSRJLIBHI&source=google-shopping&country=us&currentness=daily_updated&completeness=all_pages&key=gtin&value=%s";
	private static final boolean mockEnabled = true;
	private ConcurrentHashMap<String, String> prices = new ConcurrentHashMap<String, String>();

	public static void main(String[] args) {
		ProductPricingManagementService ppmService = new ProductPricingManagementService();
		System.out.println(ppmService.retrieveProductPriceByUpcCode("00123456000124"));
		//System.out.println(ppmService.retrieveProductPriceByUpcCode("0072151461207"));
	}

	public float retrieveProductPriceByUpcCode(String upcCode) {
		if (mockEnabled) {
			return retrieveProductPriceByUpcCodeMock(upcCode);
		} else {
			return retrieveProductPriceByUpcCodeReal(upcCode);
		}
	}

	private float retrieveProductPriceByUpcCodeMock(String upcCode) {
		float price = 0.0f;
		if (prices.containsKey(upcCode)) {
			Random rand = new Random();
			float randVal = (float) (rand.nextInt(20) + 100) / 100;
			price = Float.valueOf(prices.get(upcCode)) * randVal;
		} else {
			float minX = 5.0f;
			float maxX = 10.0f;
			Random rand = new Random();
			float finalX = rand.nextFloat() * (maxX - minX) + minX;
			price = finalX;
			prices.put(upcCode, String.format("%.2f", finalX));
		}
		return formatToTwoDigitPrecision(price);
	}

	private float formatToTwoDigitPrecision(float price) {
		String productPrice = String.format("%.2f", price);
		return Float.valueOf(productPrice);
	}

	private float retrieveProductPriceByUpcCodeReal(String upcCode) {
		HttpSecuredClientWrapper client = new HttpSecuredClientWrapper();
		String url = String.format(URL_PRODUCT_PRICING_BY_UPC, upcCode);
		Map<String, String> headers = new HashMap<>();
		headers.put(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE);

		return processJsonPayLoad(client.execute(url, headers));
	}

	private float processJsonPayLoad(String json) {
		float price = 0.0f;
		//TODO : Implement logic
		return price;
	}
}
