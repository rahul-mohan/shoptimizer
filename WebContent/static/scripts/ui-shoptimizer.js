var app = {};
app.pages = {}; //namespace for pages
app.svc = {}; //namespace for services

app.pages.listPage = {};
(function(mod){

	mod.init = function() {
		$( "#findProduct" ).on( "filterablebeforefilter", mod.findAndShowProducts);
		$( "#findProduct" ).on("click", ".addItem", mod.addItem);
		//$("#list").on("click", ".deleteItem",  mod.removeItem);
		//$("#list").on("click", ".product",  function(){ $("productDetailsPanel").panel('open');});
		$("#productDetailsPanel").on('click', function() { $('#productDetailsPanel').panel('close'); })
		$("#settings").on("click", mod.loadFromFlickr);		
	};

	mod.loadFromFlickr = function() {
		mod._flickrUrl_nature = "http://api.flickr.com/services/feeds/photos_public.gne?tags=flowers,macro,wallpaper&tagmode=all&format=json&jsoncallback=?";
		$.getJSON(mod._flickrUrl_nature,
        	function(data){
          	if(data && data.items && data.items.length!=0) {
          		var randomImageIndex = Math.floor((Math.random()*10)+1);
          		if(randomImageIndex > data.items.length) randomImageIndex = data.items.length;
          		$("#bgplaceholder").css("background-image",'url('+data.items[randomImageIndex].media.m+')');
          		 return false;
          	}
          		
        	}
        );
	};

	mod.findAndShowProducts = function ( e, data ) {
		var $ul = $( this ),
			$input = $( data.input ),
			value = $input.val(),
			html = "";

		$ul.html( "" );
		if ( value && value.length > 2 ) {
			$ul.html( "<li><div class='ui-loader'><span class='ui-icon ui-icon-loading'></span></div></li>" );
			$ul.listview( "refresh" );
			var products = app.svc.ProductCatalog.findProducts(value);	
			if(products.length>0) {
				$.each( products, function ( i, val ) {					
					html += "<li><a href='#productDetailsPanel'>" + val.name + "</a>" 
							+ "<a class='addItem' href='#' "
							+ "data-product='"+val.name+"' data-upc='"+val.upc+"' ></a></li>";
				});
				$ul.html( html );
				$ul.listview( "refresh" );
				$ul.trigger( "updatelayout");
			}
		}
	};

	//TODO: if the item is already added then show a message
	mod.addItem = function(e){
		var product = $(this).data("product");
		var upc = $(this).data("upc");

		var newItem = $("<li><a href='#'>" + product + "</a><a class='deleteItem' href='#''></a><span class='ui-li-count'>1</span></li>");
		$("#list").append(newItem);
		newItem.data("product", product);
		newItem.data("upc",upc);

		$("#list").listview("refresh");
		$('input[data-type="search"]').val("");
		$('input[data-type="search"]').trigger("keyup");
		$('input[data-type="search"]').focus();
	};

	mod.removeItem = function(e) {
		$(this).parent().remove();
	};

	mod.getItems = function() {
		var items = $("#list").children();
		var cart = jQuery.map(items, function(item,index){
			var product = {};
			product.name = $(item).data("product");
			product.upc = $(item).data("upc");
			return product;
		});
		return cart;
	};
	
	mod.getItemCodes = function() {
		var items = $("#list").children();
		var cart = jQuery.map(items, function(item,index){
			return $(item).data("upc");
		});
		return cart;
	};

}(app.pages.listPage));


//-------------------------- trip planner page ----------------------===================================================================

app.pages.tripPlannerPage = {};
(function(mod){
	
	mod.init = function(){
		$( ".tripGoLink" ).buttonMarkup({ corners: false });
		//$("#tripPlannerPage").on("pagebeforehide", mod._setSelectedPlan);
		$( ".tripGoLink" ).on('click',mod._setSelectedPlan);
		var myLocation = app.svc.Geo.getCurrentLocation();
		var zipCode = app.svc.Geo.getLocationZipCode();
		var upcCodes = app.pages.listPage.getItemCodes();
		app.svc.ShoppingPlan.buildPlan(myLocation.latitude, myLocation.longitude, zipCode,  upcCodes , mod._processResults);
	};
	 
	mod._processResults = function(data){
		var planList = $("#shoppingTripList");
		planList.empty();
		planList.data("selectedPlanIndex","-1");
		$.each(data.plans,function(i,plan){
			plan.totalRequested = app.pages.listPage.getItemCodes().length;
			var markup = $($.Mustache.render('planCard',plan));
			markup.data("plan",plan);
			markup.data("index",i);
			planList.append(markup);
		});
		planList.listview( "refresh" );
		planList.trigger( "updatelayout");
		$( ".tripGoLink" ).buttonMarkup({ corners: false });
		$( ".tripGoLink" ).on('click',mod._setSelectedPlan);
	};

	mod._setSelectedPlan = function(e){
		var index = $(e.target).parents('.tripCard').data("index");
		$("#shoppingTripList").data("selectedPlanIndex", index);
	};

	mod.getSelectedPlan = function(){
		var selectedPlanIndex = $("#shoppingTripList").data("selectedPlanIndex");	
		if(selectedPlanIndex===-1) return "NO_SELECTION";
		return $("#shoppingTripList").find("li:nth-child(" + (++selectedPlanIndex) + ")").data("plan");
	}
	
}(app.pages.tripPlannerPage));



//======================================= nearest stores page ==========================================================================

app.pages.resultsByDistancePage = {};
(function(mod){

	mod.init = function() {
		var plan = app.pages.tripPlannerPage.getSelectedPlan();
		mod._loadStores(plan.stores);
		$(".storeDetailsLink").on('click', function() { $('#storeDetailsPanel').panel('open'); } );
		$("#storeDetailsPanel").on('click', function() { $('#storeDetailsPanel').panel('close'); })
	};

	mod._loadStores = function(stores){
		if(stores === undefined || stores.length===0 ) return;
		var storeList = $("#storeList");
		storeList.empty();
		$.each(stores, function(index, store){
			var markup = $($.Mustache.render('storeList',store));
			markup.data('storeDetails',store);
			storeList.append(markup);	
		});
		storeList.listview('refresh');
		storeList.trigger('updatelayout');
	}

	mod.getStores = function () {
		var storeList = $("#storeList").children();
		var storeLocationList = $.map(storeList, function(store){
			return {
				name : $(store).data('store'),
				latitude : $(store).data('latitude'),
				longitude : $(store).data('longitude')
			};
		});
		return storeLocationList;
	}

}(app.pages.resultsByDistancePage));

//--------------------------------------------------- map page -------------------------------------------------------------------

app.pages.mapPage = {};
(function(mod){

	mod.init = function() {
		mod._showStoresInMap(); //list page should be loaded first		
	};

	mod._showStoresInMap = function() {
		var storeMarkers = app.pages.resultsByDistancePage.getStores();
		app.svc.Geo.showMap(storeMarkers);
	}

}(app.pages.mapPage));


//----------------- initialize ---------------------------=========================================================================
$(document).ready(function(){
	app.svc.Geo.init();
	$.Mustache
		.load('templates.html')
		.done(function () { 
        	console.log("Templates loaded");
    	});
});

$(document).on("pagecreate", "#listPage", 
	function(e) { 
		app.pages.listPage.init(); 
		e.preventDefault(); 
	}
);

$(document).on("pagecreate", "#tripPlannerPage", 
	function(e) { 
		app.pages.tripPlannerPage.init(); 
		e.preventDefault(); 
	}
);

$(document).on("pagecreate", "#resultsByDistancePage", 
	function(e) { 
		app.pages.resultsByDistancePage.init(); 
		e.preventDefault(); 
	}
);

$(document).on("pageshow", "#mapPage", 
	function(e) { 
		app.pages.mapPage.init(); 
		e.preventDefault(); 
	}
);

//Forward path
$(document).on("swipeleft","#listPage", function(){
	$.mobile.changePage( "#tripPlannerPage", {transition: "slide", reverse: false });
});

//Reverse path
$(document).on("swiperight","#resultsByDistancePage", function(){
	$.mobile.changePage( "#tripPlannerPage", {transition: "slide", reverse: true });
});

$(document).on("swiperight","#tripPlannerPage", function(){
	$.mobile.changePage( "#listPage", {transition: "slide", reverse: true });
});