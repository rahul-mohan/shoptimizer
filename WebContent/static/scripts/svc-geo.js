//------------------------- GeoLocator ---------------------------------------

app.svc.Geo = {};
(function(mod){

	mod.init = function() {
		if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(mod._handle_geolocation_query, mod._handle_errors);
        } else {
            yqlgeo.get('visitor', mod._normalize_yql_response);
        }
	};

	mod._handle_errors = function(error)  {
        /*switch(error.code) {
            case error.PERMISSION_DENIED: alert("user did not share geolocation data");
            break;
 
            case error.POSITION_UNAVAILABLE: alert("could not detect current position");
            break;
 
            case error.TIMEOUT: alert("retrieving position timedout");
            break;
 
            default: alert("unknown error");
            break;
        }*/

        mod._zipCode = prompt("Geolocation Failure. Please enter your zipcode");
    };
 
    mod._normalize_yql_response = function(response) {
    
        if (response.error) {
            var error = { code : 0 };
            mod._handle_errors(error);
            return;
        }
 
        var position = {
            coords :
            {
                latitude: response.place.centroid.latitude,
                longitude: response.place.centroid.longitude
            },
            address :
            {
                city: response.place.locality2.content,
                region: response.place.admin1.content,
                country: response.place.country.content
            }
        }; 
        mod._handle_geolocation_query(position);
    }
 
    mod._handle_geolocation_query = function(position) {
        var zipCode = prompt('Geolocation detected Lat: ' + position.coords.latitude + ' ' +
              'Long: ' + position.coords.longitude + '. Enter a zipcode to override');
        
        if(zipCode !== '') mod._zipCode = zipCode;

        mod._currentLocation.latitude = position.coords.latitude;
        mod._currentLocation.longitude = position.coords.longitude;
    }

    mod._currentLocation = { latitude: "NOT SET", longitude: "NOT SET"};
    mod._zipCode = "NOT_SET";

	mod.getCurrentLocation = function(){
		return mod._currentLocation;
	};

    mod.getLocationZipCode = function(){
        return mod._zipCode;
    };

	mod.showMap = function(markers){
		 var myLocation = new google.maps.LatLng(mod._currentLocation.latitude, mod._currentLocation.longitude);
		 var options = { 
    		zoom : 15, 
    		center : myLocation, 
    		mapTypeId : google.maps.MapTypeId.ROADMAP 
  		};

  		//add home location
  		if(markers === undefined) { markers = new Array(); }
  		markers.push({
			name : 'My Location',
			latitude : mod._currentLocation.latitude, 
			longitude : mod._currentLocation.longitude			
  		});

  		//initilaize map and add markers
  		$('#map_canvas').gmap(options).bind('init', function(e, map){
			$.each(markers, function(i, marker){
				var markerOption = {
					'bounds' : true,
					'position' : new google.maps.LatLng(marker.latitude, marker.longitude),
					'title' : marker.name,
					'animation' : google.maps.Animation.DROP,
					'icon' : 'http://maps.google.com/mapfiles/kml/pal3/icon18.png'
				};
				if(markerOption.title==='My Location'){
					markerOption.icon = 'http://maps.google.com/mapfiles/kml/pal3/icon48.png';
				}
				$('#map_canvas').gmap('addMarker', markerOption).click(function(){
                    var infoContent = { 'content': '<table class="infoBoxTable"><tr><td>Store: </td><td>' + markerOption.title 
                            + ' </td></tr><tr><td>Availability: </td><td>3/7</td></tr></table>' };    

                    /*var infoContent = { 'content': '<div>Store: ' + markerOption.title 
                            + ' </div>&nbsp;<div>Availability: </div>3/7' };*/
					$('#map_canvas').gmap('openInfoWindow', infoContent	, this);
				});
			});  				
  		});  		     	
	};

}(app.svc.Geo));