//--------------- ShoppingPlan Service ---------------------------------------

app.svc.ShoppingPlan = {};
(function(mod){
	
	mod._buildPlanURL = "../../buildShoppingPlan";
	
	mod.buildPlan = function(lat,long, zip, upcCodes, processResults){

		var items = jQuery.map(upcCodes,function(item, index){
			return {
    			"upcCode" : item, 
    			"quantity" : "1"
    			};
		});

		/*$.getJSON( mod._buildPlanURL,  
				JSON.stringify({ 'items' : items, 'latitude': lat, 'longitude' : long, 'zipCode': zip}) //data
			)
			.done(processResults) //call process results on success
			.fail(function() { app.svc.ErrorHandler.handle("buildShoppingPlan"); });*/


		var jsonData = JSON.stringify({ 'items' : items, 'latitude': lat, 'longitude' : long, 'zipCode': zip});

		var settings = {
			type: "GET",
			/*accept: 'application/json',
			contentType : 'application/json',*/
			data : {
				payload : jsonData
			} ,
			dataType: 'json',
			success: function(data, txtStatus, jqXHR){
				console.log(txtStatus);
				processResults(data);
			},
			error: function() { 
				console.log('error');
				app.svc.ErrorHandler.handle("buildShoppingPlan"); 
			}
		};

		$.ajax(mod._buildPlanURL, settings);
	};
	
}(app.svc.ShoppingPlan));

//---------------- Error Handler -------------------------
app.svc.ErrorHandler = {};
(function(mod){

	mod.handle = function(msg){
		alert("ServiceInvocationFailure: " + msg);
	};
	
}(app.svc.ErrorHandler));
